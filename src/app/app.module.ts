import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FeaturesComponent } from './components/features/features.component';
import { CurrenciesComponent } from './components/currencies/currencies.component';
import { NoPageComponent } from './components/no-page/no-page.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { LoginPageModule } from './components/login/login.module';
import { NeedAuthGuard } from './services/auth.guard';
import { HttpModule } from '@angular/http';
import { CurrencyUsdComponent } from './components/currencies/currency-usd/currency-usd.component';
import { CurrencyPlnComponent } from './components/currencies/currency-pln/currency-pln.component';

@NgModule({
  declarations: [
    AppComponent,
    FeaturesComponent,
    CurrenciesComponent,
    NoPageComponent,
    CurrencyUsdComponent,
    CurrencyPlnComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LoginPageModule,
    HttpModule
  ],
  providers: [
    NeedAuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
