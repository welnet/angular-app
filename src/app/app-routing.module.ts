import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './components/login/login.component';
import { FeaturesComponent } from './components/features/features.component';
import { CurrenciesComponent } from './components/currencies/currencies.component';
import { NoPageComponent } from './components/no-page/no-page.component';
import { NeedAuthGuard } from './services/auth.guard';

const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'features', component: FeaturesComponent, canActivate: [NeedAuthGuard] },
    { path: 'currencies', component: CurrenciesComponent, canActivate: [NeedAuthGuard] },
    { path: '404', component: NoPageComponent },
    { path: '**', redirectTo: '/404' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {
}
