import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { CustomerService } from 'src/app/services/customer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  email = 'marcin@welenc';
  password = 'Test1234';

  constructor(
    private api: ApiService,
    private customer: CustomerService,
    private router: Router
  ) { }

  tryLogin() {
    this.api.login(
      this.email,
      this.password
    )
    .subscribe(
      r => {
        if (r.token) {
          this.customer.setToken(r.token);
          this.router.navigateByUrl('/features')
        }
      },
      r => {
        alert(r.error.error);
      }
    );
  }

}
