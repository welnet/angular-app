import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-currency-pln',
  templateUrl: './currency-pln.component.html',
  styleUrls: ['./currency-pln.component.scss']
})
export class CurrencyPlnComponent {
  private apiUrl = 'https://bitbay.net/API/Public/BTCPLN/ticker.json';
  data: any = {}

  constructor(
    private http: Http 
  ) {
    this.getCurrencies();
    this.getData();
   }

   getData() {
     return this.http.get(this.apiUrl)
     .pipe(map((res: Response) => res.json()))
   }

   getCurrencies() {
     this.getData().subscribe(data => {
       console.log(data);
       this.data = data;
     })
   }

   showValueInUsd() {
     const valueInUsd = this.data.average / 3;
     return valueInUsd;
   }
}
