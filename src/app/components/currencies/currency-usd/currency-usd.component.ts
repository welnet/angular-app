import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-currency-usd',
  templateUrl: './currency-usd.component.html',
  styleUrls: ['./currency-usd.component.scss']
})
export class CurrencyUsdComponent {
  private apiUrl = 'https://bitbay.net/API/Public/BTCPLN/ticker.json';
  data: any = {}

  constructor(
    private http: Http 
  ) {
    this.getCurrencies();
    this.getData();
   }

   getData() {
     return this.http.get(this.apiUrl)
     .pipe(map((res: Response) => res.json()))
   }

   getCurrencies() {
     this.getData().subscribe(data => {
       console.log(data);
       this.data = data;
     })
   }

}
